# Occam Web Client Memento Protocol Plugin

This plugin adds the [Memento Protocol](https://mementoweb.org/guide/quick-intro/)
to an instance of Occam. This protocol adds common sense support for negotiating
an archived digital object by a date when that content was living and accessible.

To install to your Occam instance, refer to the section on "Installing to an
Occam Instance" below.

## Added Functionality

The **Memento Protocol** adds additional negotiation of digital objects via its
additonal `Accept-Date` HTTP field. Optionally, this can be offered in a
non-standard form using the `from` query parameter. That is, any Occam URL such
as `<ID>/<REVISION>?from=Sun, 28 Feb 1993 05:00:00 GMT` will then be the
equivalent to having `Accent-Date: Sun, 28 Feb 1993 05:00:00 GMT` in the HTTP
headers for the request. Either way, the content will be served via a redirect
to the revision of the digital object that is at least as old as the requested
date. 

It also adds a route off of the object URL that generates the Memento "timemap"
link document of type `application/link-format`. This is used to quickly query
the possible snapshots of the digital object at particular dates and their
corresponding URLs. This URL is ofInteracting  the form `<ID>/memento/timemap`.

## Additional Memento-Tracer Support

The plugin also enables features useful in the ingest and replay of Tracer
web archival traces. Right now, these features are in development alongside
Tracer itself. This section will be updated as these features are expanded
and completed.

## Installing to an Occam Instance

Just placing this repository inside the `plugins` directory of the Occam Web
Client repository will automatically enable it. That is, like other plugins,
if the repo exists in the `<occam-web-client root>/plugins/occam-web-memento`
path, then it will enable the functionality.

## Files

`controllers/object.rb` - Overrides `resolveObject` and the generic Rack
`call` method to override the normal URL handling of all object routes. This
will handle the `from=` and `Accept-Date` negotiation.

`controllers/timemap.rb` - Adds the `memento/timemap` route to provide the link
map needed by the protocol.
