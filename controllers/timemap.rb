# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2021 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Occam
  module Plugins
    module Memento
      module Controllers
        class MementoTimemapController < Occam::Controllers::ObjectController
          # Retrieve the timemap link chain
          get %r{#{OBJECT_ROUTE_REGEX}/memento/timemap/?} do
            # Get a reference to the object itself
            @object = resolveObject()

            # Get the versions for the object and use any that are parsed as
            # dates.
            versions = @object.versions(:json => true)[:versions]
            versions = versions.map do |version|
              begin
                if version[:version][0] == "."
                  version[:version] = Time.iso8601(version[:version][1...]).utc
                  version
                else
                  nil
                end
              rescue
                nil
              end
            end.filter do |version|
              !version.nil?
            end.sort do |a, b|
              a[:version] <=> b[:version]
            end

            # Return a 404 when the timemap is empty
            if versions.count == 0
              status 404
              return
            end

            # Format the list to a 'application/link-format'
            content_type 'application/link-format'

            # Create the link map
            links = []

            # The 'original' link is a URL to the original resource (or this page, if self contained)
            links.push "<#{@object.info[:source] || (request.scheme + "://" + request.host_with_port + @object.url(:short => true))}>; rel=\"original\""

            # The 'self' rel points back to this page
            links.push "<#{request.scheme + "://" + request.host_with_port + @object.url(:short => true, :path => "memento/timemap")}>; rel=\"self\"; type=\"application/link-format\"; from=\"#{versions[0][:version].rfc822.gsub(/[+-]0000/, "GMT")}\"; until=\"#{versions[-1][:version].rfc822.gsub(/[+-]0000/, "GMT")}\""

            # The 'timegate' points to the root archived resource, which is the
            # object itself here in Occam.
            links.push "<#{request.scheme + "://" + request.host_with_port + @object.url(:short => true)}>; rel=\"timegate\""

            # Every revision URL is then posted in the map for each known date
            # tracked by the version tag that is iso8601 parsable.
            versions.each do |tag|
              links.push "<#{request.scheme + "://" + request.host_with_port + @object.url(:revision => tag[:revision])}>; rel=\"memento\"; datetime=\"#{tag[:version].rfc822.gsub(/[+-]0000/, "GMT")}\""
            end

            links.join(",\n") + ","
          end
        end
      end
    end
  end

  use Plugins::Memento::Controllers::MementoTimemapController
end
