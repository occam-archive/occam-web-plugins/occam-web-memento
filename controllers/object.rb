# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2021 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Occam
  module Controllers
    class ObjectController < Occam::Controller
      if not method_defined?(:callPreMemento)
        alias_method :callPreMemento, :call
      end

      # Overrides `call` to make use of the 'from' query parameter to force Accept-Date.
      def call(env)
        request = Rack::Request.new(env)

        # Allow Accept-Date to be provided from the 'from' query parameter
        if request.params["from"]
          request.env["HTTP_ACCEPT_DATE"] = request.params["from"]
        end

        # Just carry on
        callPreMemento(env)
      end

      # Keep the old version of resolveObject around
      if not method_defined?(:resolveObjectPreMemento)
        alias_method :resolveObjectPreMemento, :resolveObject
      end

      # Overrides `resolveObject` to directly set revision based on Accept-Date.
      def resolveObject(*args)
        # Negotiate based on Access-Date, if provided
        # So, we override the revision when necessary
        begin
          from = Time.parse(request.env["HTTP_ACCEPT_DATE"])
        rescue
          from = nil
        end

        # Resolve to object
        @object = resolveObjectPreMemento(*args)

        # Determine the revision to override using 'from' and redirect appropriately
        if from
          # Look at all timestamp versions
          versions = @object.versions(:json => true)[:versions]
          versions = versions.map do |version|
            begin
              if version[:version][0] == "."
                version[:version] = Time.iso8601(version[:version][1...]).utc
                version
              else
                nil
              end
            rescue
              nil
            end
          end.filter do |version|
            !version.nil?
          end.sort do |a, b|
            a[:version] <=> b[:version]
          end

          # Find the one that is older than the reference
          matched = nil
          versions.each do |version|
            if matched.nil? && version[:version] >= from
              matched = version
            end
          end

          # Set revision (if it differs)
          if matched && params[:revision] != matched[:revision]
            params[:revision] = matched[:revision]

            # Re-resolve
            @object = resolveObjectPreMemento(*args)

            # Redirect to that object instead
            redirect @object.url(:path => request.path.gsub(/^\/[^\/]+\/[^\/]+\/?/, ''))

            # Do not resolve normally
            halt
          end
        end

        @object
      end
    end
  end
end
